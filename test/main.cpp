//Name:Chen Xu
//ID:X287284
//Email:xu.c@icloud.com
//Ray tracing global illumination render:use super sampling(random ray for each pixel)
//Implement features: 3 different surface(diffuse, mirror reflection, glass)
//The plane is defined as a sphere with very large radius,so it looks like a plane.
//The light is defined as a small radius,but have large emission parameter.
//So all objects can be store in a Sphere array for tranverse:spheres[]
//All lines are well marked

#include <math.h>
#include <stdlib.h>
#include <stdio.h>

struct Vec {
    double x, y, z;                  // position, also color (r,g,b)
    Vec(double x_=0, double y_=0, double z_=0){ x=x_; y=y_; z=z_; }
    Vec operator+(const Vec &b) const { return Vec(x+b.x,y+b.y,z+b.z); }
    Vec operator-(const Vec &b) const { return Vec(x-b.x,y-b.y,z-b.z); }
    Vec operator*(double b) const { return Vec(x*b,y*b,z*b); }
    Vec mult(const Vec &b) const { return Vec(x*b.x,y*b.y,z*b.z); }
    Vec& norm(){ return *this = *this * (1/sqrt(x*x+y*y+z*z)); }
    double length(){return sqrt(x*x+y*y+z*z);}
    double dot(const Vec &b) const { return x*b.x+y*b.y+z*b.z; } // cross:
    Vec operator%(Vec&b){return Vec(y*b.z-z*b.y,z*b.x-x*b.z,x*b.y-y*b.x);}
};

struct Ray {
    Vec o, d;//pos,direction
    Ray(Vec o_, Vec d_) : o(o_), d(d_) {}
    
};

enum Refl_t {
    DIFF, //solid
    SPEC, //reflect
    REFR  //glass
};  // material types, used in radiance()

struct Sphere {
    double rad;       // radius
    Vec p, e, c;      // position, emission, color
    Refl_t refl;      // reflection type (DIFFuse, SPECular, REFRactive)
    
    //constructer
    Sphere(double rad_, Vec p_, Vec e_, Vec c_, Refl_t refl_):
    rad(rad_), p(p_), e(e_), c(c_), refl(refl_) {}
    
    //check if collsion
    double intersect(const Ray &r) const { // returns distance, 0 if nohit
        // Solve t^2*d.d + 2*t*(o-p).d + (o-p).(o-p)-R^2 = 0
        Vec op = p-r.o;
        double t, eps=1e-4, b=op.dot(r.d), det=b*b-op.dot(op)+rad*rad;
        
        //ray miss sophere
        if (det<0)
            return 0;
        else
            det=sqrt(det);
        return (t=b-det)>eps ? t : ((t=b+det)>eps ? t : 0);
    }
};

struct Plane {
    double w,h;  //width,height
    Vec center;
    Vec headup;
    Vec normal;
    Vec color;
    Refl_t refl;
    Plane(double w_,double h_,Vec center_,Vec headup_,Vec normal_,Vec color_,Refl_t refl_):
    w(w_),h(h_),center(center_),headup(headup_),normal(normal_),color(color_),refl(refl_){}
    
};

//  Draw scene,every object is based on sphere
//  Sphere(radius,position,emission,color,material)

Sphere spheres[] = {
    Sphere(1e5, Vec( 1e5+1,40.8,81.6), Vec(),Vec(0.25,0.25,1),DIFF),//Left wall
    Sphere(1e5, Vec(-1e5+99,40.8,81.6),Vec(),Vec(1,0.25,0.25),DIFF),//Right wall
    //Sphere(1e5, Vec(50,40.8, 1e5),     Vec(),Vec(.75,.75,.75),DIFF),//Back wall
    //Sphere(1e5, Vec(50,40.8,-1e5+170), Vec(),Vec(),           DIFF),//Front wall
    Sphere(1e5, Vec(50, 1e5, 81.6),    Vec(),Vec(.75,.75,.75),DIFF),//Bottom wall
    Sphere(1e5, Vec(50,-1e5+81.6,81.6),Vec(),Vec(.75,.75,.75),DIFF),//Top wall
    
    Sphere(16.5,Vec(27,16.5,47),       Vec(),Vec(1,1,1)*.999, DIFF),//Diffuse
    Sphere(16.5,Vec(50,35,78),         Vec(),Vec(1,1,1)*.999, REFR),//Glass
    Sphere(16.5,Vec(80,16.5,90),       Vec(),Vec(1,1,1)*.999, SPEC),//Mirror

    Sphere(1, Vec(20,70,81.6),Vec(0.75,0.25,0.25)*500,  Vec(), DIFF),//Light
    Sphere(1, Vec(50,70,81.6),Vec(4,4,4)*300,  Vec(), DIFF),//Light
    Sphere(1, Vec(80,70,81.6),Vec(0.25,0.25,0.75)*500,  Vec(), DIFF),//Light
};

//bound x to 0-1
inline double clamp(double x){ return x<0 ? 0 :(x>1 ? 1 : x); }

//convert color from range 0-1 to range 0-255
inline int toInt(double x){ return int(pow(clamp(x),1/2.2)*255+.5); }


//check whether this sphere intersect with ray r
inline bool intersect(const Ray &r, double &t, int &id){
    
    //number of sphreres
    double n=sizeof(spheres)/sizeof(Sphere);
    double d;
    double inf=t=1e20;
    
    for(int i=int(n);i--;){
        if((d=spheres[i].intersect(r)) && d<t){
            t=d;
            id=i;
        }
    }
    return t<inf;
}

int numSpheres = sizeof(spheres)/sizeof(Sphere);

Vec radiance2(const Ray &r, int depth, unsigned short *Xi,int E=1){
    double t;                               // distance to intersection
    int id=0;                               // id of intersected object
    if (!intersect(r, t, id))
        return Vec(); // if miss, return black
    const Sphere &obj = spheres[id];        // the hit object
    Vec x=r.o+r.d*t, n=(x-obj.p).norm(), nl=n.dot(r.d)<0?n:n*-1, f=obj.c;
    double p = f.x>f.y && f.x>f.z ? f.x : f.y>f.z ? f.y : f.z; // max refl
    
//    if (++depth>2||!p){
//        if (erand48(Xi)<p){
//            f=f*(1/p);
//        }
//        else{
//            return obj.e*E;
//        }
//    }
    if (++depth > 8) {
        return obj.e*E;
    }

    
    if (obj.refl == DIFF){                  // Ideal DIFFUSE reflection
        
        //random reflect
        //double r1=2*M_PI*erand48(Xi), r2=erand48(Xi), r2s=sqrt(r2);
        //Vec w=nl, u=((fabs(w.x)>.1?Vec(0,1):Vec(1))%w).norm(), v=w%u;
        //Vec d = (u*cos(r1)*r2s + v*sin(r1)*r2s + w*sqrt(1-r2)).norm();
        
        
        
        // Loop over every light
        Vec e;
        for (int i=0; i<numSpheres; i++){
            const Sphere &s = spheres[i];
            if (s.e.x<=0 && s.e.y<=0 && s.e.z<=0) continue; // skip non-lights
            
            Vec sw=s.p-x, su=((fabs(sw.x)>.1?Vec(0,1):Vec(1))%sw).norm(), sv=sw%su;
            double cos_a_max = sqrt(1-s.rad*s.rad/(x-s.p).dot(x-s.p));
            double eps1 = 0, eps2 = 0;
            double cos_a = 1-eps1+eps1*cos_a_max;
            double sin_a = sqrt(1-cos_a*cos_a);
            double phi = 2*M_PI*eps2;
            Vec l = su*cos(phi)*sin_a + sv*sin(phi)*sin_a + sw*cos_a;
            l.norm();
            
            //cast shadow
            if (intersect(Ray(x,l), t, id) && id==i){
                double omega = 2*M_PI*(1-cos_a_max);
                e = e + f.mult(s.e*l.dot(nl)*omega)*M_1_PI;  // 1/pi for brdf
            }
        }
        
        if (depth>0) return obj.e*E + e;

        Vec d = r.d-n*2*n.dot(r.d);
        return obj.e*E + e + f.mult(radiance2(Ray(x,d),depth,Xi,0));
    }
    else if (obj.refl == SPEC)              // Ideal SPECULAR reflection
        return obj.e + f.mult(radiance2(Ray(x,r.d-n*2*n.dot(r.d)),depth,Xi));
    
    else{
        Ray reflRay(x, r.d-n*2*n.dot(r.d));     // Ideal dielectric REFRACTION
        bool into = n.dot(nl)>0;                // Ray from outside going in?
        double nc=1, nt=1.5, nnt=into?nc/nt:nt/nc, ddn=r.d.dot(nl), cos2t;
        if ((cos2t=1-nnt*nnt*(1-ddn*ddn))<0)    // Total internal reflection
            return obj.e + f.mult(radiance2(reflRay,depth,Xi));
        
        Vec tdir = (r.d*nnt - n*((into?1:-1)*(ddn*nnt+sqrt(cos2t)))).norm(); //refracted ray
        double a=nt-nc, b=nt+nc, R0=a*a/(b*b), c = 1-(into?-ddn:tdir.dot(n));
        double Re=R0+(1-R0)*c*c*c*c*c;           //fresnel reflectance
        double Tr=1-Re,P=.25+.5*Re,RP=Re/P,TP=Tr/(1-P);
        //return obj.e + f.mult(depth>2 ? (erand48(Xi)<P ? radiance2(reflRay,depth,Xi)*RP:radiance2(Ray(x,tdir),depth,Xi)*TP) : radiance2(reflRay,depth,Xi)*Re+radiance2(Ray(x,tdir),depth,Xi)*Tr);
        //return obj.e + f.mult(depth>11 ? (radiance2(Ray(x,tdir),depth,Xi)*TP) : radiance2(reflRay,depth,Xi)*Re+radiance2(Ray(x,tdir),depth,Xi)*Tr);
       // return obj.e + f.mult(depth>10 ? (radiance2(reflRay,depth,Xi)*RP) : radiance2(reflRay,depth,Xi)*Re+radiance2(Ray(x,tdir),depth,Xi)*Tr);
        return obj.e + f.mult(depth>1 ? (radiance2(reflRay,depth,Xi)*RP + radiance2(Ray(x,tdir),depth,Xi)*TP) : radiance2(reflRay,depth,Xi)*Re+radiance2(Ray(x,tdir),depth,Xi)*Tr);

    }
    
}




//cast direct ray
void nosupersampling(){
    int w=640, h=640;                                           //picture size
    int samplewidth = 2;
    int samps = samplewidth*samplewidth;                        // # samples
    Vec cam(50,50,150);
    
    Vec r;                                                      // colors for samples
    Vec *c=new Vec[w*h];                                        // store the image
    
    for (int y=0; y<h; y++){                                    // Loop over image rows
        fprintf(stderr,"\rRendering (%d spp) %5.2f%%",samps*4,100.*y/(h-1));
        for (unsigned short x=0, Xi[3]={0,0,static_cast<unsigned short>(y*y*y)}; x<w; x++)  { // Loop cols
            for (int j= 0; j<samplewidth; j++) {
                Vec dirction = Vec(x-w/2,h/2-y,-200);
                //cast ray
                Ray ray = Ray(cam+dirction.norm(),dirction);
                r = Vec();
                //accumulate color
                r = r + radiance2(ray,0,Xi);
                int i = y*w+x;
                c[i] = c[i] + Vec(clamp(r.x),clamp(r.y),clamp(r.z))*.25;
            }
        }
    }
    FILE *f = fopen("nosampling.ppm", "w");         // Write image to PPM file.
    fprintf(f, "P3\n%d %d\n%d\n", w, h, 255);
    for (int i=0; i<w*h; i++)
        fprintf(f,"%d %d %d ", toInt(c[i].x), toInt(c[i].y), toInt(c[i].z));
}

//cast random ray
//about 5x slower, but its anti-alised
void supersamlping(){
    int samp = 2;
    int w=640, h=640, samps =samp*samp;                                                     // # samples
    Ray cam(Vec(50,52,295.6), Vec(0,-0.042612,-1).norm());                                  // cam pos, dir
    Vec cx=Vec(w*.5135/h), cy=(cx%cam.d).norm()*.5135, r, *c=new Vec[w*h];                  //increment in x,y direction

    for (int y=0; y<h; y++){                                                                // Loop over image rows
        fprintf(stderr,"\rRendering (%d spp) %5.2f%%",samps*4,100.*y/(h-1));
        for (unsigned short x=0, Xi[3]={0,0,static_cast<unsigned short>(y*y*y)}; x<w; x++)  // Loop cols
            for (int sy=0, i=(h-y-1)*w+x; sy<samp; sy++)                                    //  subpixel rows
                for (int sx=0; sx<samp; sx++, r=Vec()){                                     //  subpixel cols
                    for (int s=0; s<samps; s++){
                        double
                        r1=2*erand48(Xi), dx=r1<1 ? sqrt(r1)-1: 1-sqrt(2-r1);
                        double r2=2*erand48(Xi), dy=r2<1 ? sqrt(r2)-1: 1-sqrt(2-r2);
                        Vec d = cx*( ( (sx+.5 + dx)/2 + x)/w - .5) +
                                cy*( ( (sy+.5 + dy)/2 + y)/h - .5) + cam.d;                 //random direction
                        //accumulate color
                        r = r + radiance2(Ray(cam.o+d*140,d.norm()),0,Xi)*(1./samps);
                    }
                    c[i] = c[i] + Vec(clamp(r.x),clamp(r.y),clamp(r.z))*.25;
                }
    }
    FILE *f = fopen("supersampling.ppm", "w");         // Write image to PPM file.
    fprintf(f, "P3\n%d %d\n%d\n", w, h, 255);
    for (int i=0; i<w*h; i++)
        fprintf(f,"%d %d %d ", toInt(c[i].x), toInt(c[i].y), toInt(c[i].z));
}

int main(int argc, char *argv[]){
    //supersamlping();
    nosupersampling();
}